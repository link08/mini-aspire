<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->middleware('auth:api')
    ->group(function() {

    $sections = ['user', 'fund', 'loan', 'repayment'];
    foreach ($sections as $section) {
        $sectionController = ucfirst(camel_case($section)) . 'Controller';
        Route::resource($section, $sectionController);
    }

    #List of loan repayments
    Route::get('loan/{loan}/repayments', 'LoanController@repayments');
    #Pay a repayment
    Route::put('repayment/{repayment}/pay', 'RepaymentController@pay');

});
