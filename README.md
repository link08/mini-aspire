# Mini Aspire Project
Created by: Lunardi Loris
Git: [https://bitbucket.org/link08/mini-aspire](https://bitbucket.org/link08/mini-aspire)

## Setup

### Init

List of command for setup the project:

``` shell
composer install
php artisan key:generate
php artisan migrate --seed
php artisan passport:install
```

## Testing

### Postman

Import the file *./Mini_Aspire.postman_collection.json* and set the current enviroment variables to test the code:

| Variable      | Source           |
| ------------- |:-------------:|
| url           | .env file      |
| client_id     | Generated with *php artisan passport:install* command |
| client_secret | Generated with *php artisan passport:install* command |
| access_token  | Returned with the first login call |

### Php UnitTest

``` shell
phpunit
```
The DDT test the following features:

* A Loan creation failure
* A Loan creation successfully
* Show and Check Loan data
* Show and Check Repayments automatically generated
* Set as paid a Reapyment
