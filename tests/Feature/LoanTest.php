<?php

namespace Tests\Feature;

use App\User;
use App\Fund;
use App\Loan;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoanTest extends TestCase
{
    use RefreshDatabase;

    protected static $superUser;
    protected static $user;

    public function setUp()
    {
        parent::setup();
        self::$superUser = factory(User::class)->create([
            'is_superuser' => 1,
        ]);
        self::$user = factory(User::class)->create();
    }

    public function testLoanCreateFailTest()
    {
        $response = $this->actingAs(self::$superUser, 'api')
            ->json('POST', '/api/v1/loan', [
                'user_id' => static::$user->id,
                'title' => 'Loan test',
                'installaments_number' => 12,
                'frequency' => 1,
                'amount' => 1000,
                'interest_rate' => 10,
                'arrangement_fee' => 100,

            ]);

        $response
            ->assertStatus(422)
            ->assertJsonValidationErrors(['amount'])
            ->assertJson([
                'errors' => [
                    'amount' => ['amount not available.'],
                ]
            ]);
    }

    public function testLoanCreateSuccessfullyTest()
    {
        factory(Fund::class)->create(['amount' => 1000]);

        $response = $this->actingAs(self::$superUser, 'api')
            ->json('POST', '/api/v1/loan', [
                'user_id' => static::$user->id,
                'title' => 'Loan test',
                'installaments_number' => 12,
                'frequency' => 1,
                'amount' => 900,
                'interest_rate' => 10,
                'arrangement_fee' => 100,

            ]);

        $response
            ->assertStatus(201)
            ->assertJson([
                'message' => 'Created'
            ]);
    }

    public function testLoanShowTest()
    {
        $loan = factory(Loan::class)->create([
            'amount' => 900,
            'interest_rate' => 10,
            'arrangement_fee' => 100
            ]);

        $response = $this->actingAs(self::$superUser, 'api')
            ->json('GET', '/api/v1/loan/' . $loan->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'total_amount' => 1090,
                    'total_paid' => 0,
                    'total_unpaid' => 1090
                ]
            ]);
    }

    public function testLoanRepaymentsShowTest()
    {
        $loan = factory(Loan::class)->create([
            'installaments_number' => 10,
            'amount' => 900,
            'interest_rate' => 10,
            'arrangement_fee' => 110
            ]);

        $response = $this->actingAs(self::$superUser, 'api')
            ->json('GET', '/api/v1/loan/' . $loan->id . '/repayments');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    0 => [
                        'amount' => 110
                    ]
                ]
            ]);
    }

    public function testLoanRepaymentSetAsPaidTest()
    {
        $loan = factory(Loan::class)->create([
            'installaments_number' => 10,
            'amount' => 900,
            'interest_rate' => 10,
            'arrangement_fee' => 110
        ]);
        $wrongUser = factory(User::class)->create();
        $repayment = $loan->repayments->first();

        // Try to pay the repayment by the wrong User
        $response = $this->actingAs($wrongUser, 'api')
            ->json('PUT', '/api/v1/repayment/' . $repayment->id . '/pay', ['payment_method_id' => 1]);

        $response->assertStatus(404);

        // Correct payment action
        $response = $this->actingAs(self::$user, 'api')
            ->json('PUT', '/api/v1/repayment/' . $repayment->id . '/pay', ['payment_method_id' => 1]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Paid'
            ]);

        // Try to repay the repayment with the correct user
        $response = $this->actingAs(self::$user, 'api')
            ->json('PUT', '/api/v1/repayment/' . $repayment->id . '/pay', ['payment_method_id' => 1]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Repayment already paid'
            ]);

        // Check the correct total amount
        $response = $this->actingAs(self::$superUser, 'api')
            ->json('GET', '/api/v1/loan/' . $loan->id);

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'total_amount' => 1100,
                    'total_paid' => 110,
                    'total_unpaid' => 990
                ]
            ]);
    }
}
