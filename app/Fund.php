<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    protected $fillable = ['author', 'description', 'amount'];

    static function getTotalAmount()
    {
        return self::get()->sum('amount');
    }

    static function getResidualAmount()
    {
        return self::getTotalAmount() - \App\Loan::getTotalAmount();
    }
}
