<?php

namespace App;

use App\Scopes\OrderScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Repayment extends Model
{
    use SoftDeletes;

    protected $dates = ['due_date', 'paid_at', 'created_at'];

    protected $fillable = [
        'title', 'description', 'amount', 'due_date',
        'loan_id', 'payment_method_id', 'paid_at'
    ];

    const PAYMENT_METHOD = [
        1 => 'Bank transfer',
        2 => 'PayPal',
        3 => 'Credit Cards'
    ];

    protected static function boot()
    {
        parent::boot();

        // Order by due date
        static::addGlobalScope(new OrderScope('due_date', 'asc'));

        // Show only the abailable Element
        static::addGlobalScope('available', function(Builder $builder) {
            $user = auth()->user();
            if ( $user && ! $user->is_superuser ) {
                $builder->whereHas('loan', function ($query) use ($user) {
                    $query->where('user_id', $user->id);
                });
            }

            return $builder;
        });
    }

    public function loan()
    {
        return $this->belongsTo('App\Loan');
    }

    public function scopePaid($query)
    {
        return $query->whereNotNull('paid_at');
    }

    public function scopeUnpaid($query)
    {
        return $query->whereNull('paid_at');
    }

    public function getPaymentMethodAttribute()
    {
        return isset(self::PAYMENT_METHOD[$this->attributes['payment_method_id']]) ? self::PAYMENT_METHOD[$this->attributes['payment_method_id']] : null;
    }

}
