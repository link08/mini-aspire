<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'email_verified_at',
        'password', 'last_login_at', 'disabled_at'
    ];

    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['last_login_at', 'disabled_at'];

    public function loans()
    {
        return $this->hasMany('App\Loan');
    }

    public function getDisabledAttribute()
    {
        return (bool) ! is_null($this->disabled_at);
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

}
