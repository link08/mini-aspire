<?php

namespace App\Http\Controllers;

use App\Fund;
use App\Http\Requests\FundRequest;
use App\Http\Resources\FundCollection;
use App\Http\Resources\Fund as FundResource;

class FundController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Fund::class);
    }

    public function index()
    {
        return new FundCollection(Fund::get());
    }

    public function create()
    {
        //
    }

    public function store(FundRequest $request)
    {
        $fund = Fund::create($request->all());

        return response()->json([
            'id' => $fund->id,
            'code' => 201,
            'message' => 'Created'
        ], 201);
    }

    public function show(Fund $fund)
    {
        return new FundResource($fund);
    }

    public function edit(Fund $fund)
    {
        //
    }

    public function update(FundRequest $request, Fund $fund)
    {
        $fund->update($request->all());

        return response()->json([
            'code' => 200,
            'message' => 'Updated'
        ]);
    }

    public function destroy(Fund $fund)
    {
        $fund->delete();

        return response()->json([
            'code' => 200,
            'message' => 'Deleted'
        ]);
    }
}
