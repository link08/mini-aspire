<?php

namespace App\Http\Controllers;

use App\Repayment;
use App\Http\Requests\RepaymentRequest;
use App\Http\Requests\RepaymentPayRequest;
use App\Http\Resources\RepaymentCollection;
use App\Http\Resources\Repayment as RepaymentResource;

class RepaymentController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Repayment::class);
    }

    public function index()
    {
        return new RepaymentCollection(Repayment::with('loan')->get());
    }

    public function create()
    {
        //
    }

    public function store(RepaymentRequest $request)
    {
        $repayment = Fund::create($request->all());

        return response()->json([
            'id' => $repayment->id,
            'code' => 201,
            'message' => 'Created'
        ]);
    }

    public function show(Repayment $repayment)
    {
        return new RepaymentResource($repayment);
    }

    public function edit(Repayment $repayment)
    {
        //
    }

    public function update(RepaymentRequest $request, Repayment $repayment)
    {
        $repayment->update($request->all());

        return response()->json([
            'code' => 200,
            'message' => 'Updated'
        ]);
    }

    public function destroy(Repayment $repayment)
    {
        $repayment->delete();

        return response()->json([
            'code' => 200,
            'message' => 'Deleted'
        ]);
    }

    public function pay(RepaymentPayRequest $request, Repayment $repayment)
    {
        $this->authorize('pay', $repayment);

        if ($repayment->paid_at) {
            return response()->json([
                'code' => 200,
                'message' => 'Repayment already paid'
            ]);
        }

        $request->merge(['paid_at' => now()]);
        $repayment->update($request->all());

        return response()->json([
            'code' => 200,
            'message' => 'Paid'
        ]);
    }
}
