<?php

namespace App\Http\Controllers;

use App\Loan;
use App\Http\Requests\LoanRequest;
use App\Http\Resources\LoanCollection;
use App\Http\Resources\RepaymentCollection;
use App\Http\Resources\Loan as LoanResource;

class LoanController extends Controller
{

    public function __construct()
    {
        $this->authorizeResource(Loan::class);
    }

    public function index()
    {
        return new LoanCollection(Loan::get());
    }

    public function create()
    {
        //
    }

    public function store(LoanRequest $request)
    {
        $loan = Loan::create($request->all());

        return response()->json([
            'id' => $loan->id,
            'code' => 201,
            'message' => 'Created'
        ], 201);
    }

    public function show(Loan $loan)
    {
        return new LoanResource($loan);
    }

    public function edit(Loan $loan)
    {
        //
    }

    public function update(LoanRequest $request, Loan $loan)
    {
        $loan->update($request->all());

        return response()->json([
            'code' => 200,
            'message' => 'Updated'
        ]);
    }

    public function destroy(Loan $loan)
    {
        $loan->delete();

        return response()->json([
            'code' => 200,
            'message' => 'Deleted'
        ]);
    }

    public function repayments(Loan $loan)
    {
        return new RepaymentCollection($loan->repayments);
    }
}
