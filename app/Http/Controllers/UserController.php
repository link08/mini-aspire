<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class);
    }

    public function index()
    {
        return new UserCollection(User::get());
    }

    public function create()
    {
        //
    }

    public function store(UserRequest $request)
    {
        $user = User::create($request->all());

        return response()->json([
            'id' => $user->id,
            'code' => 201,
            'message' => 'Created'
        ], 201);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }

    public function edit(User $user)
    {
        //
    }

    public function update(UserRequest $request, User $user)
    {
        $user->update($request->all());

        return response()->json([
            'code' => 200,
            'message' => 'Updated'
        ]);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json([
            'code' => 200,
            'message' => 'Deleted'
        ]);
    }
}
