<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection AS LaravelResourceCollection;

class ResourceCollection extends LaravelResourceCollection
{
    private $options;

    public function __construct($resource, $options = NULL)
    {
        parent::__construct($resource);
        $this->resource = $resource;

        $this->options = is_array($options) ? $options : [];
    }

    public function toArray($request)
    {
        return array_merge(['data' => $this->collection], $this->options);
    }

    public function with($request)
    {
        return [
            'code' => 200,
            'message' => 'Ok',
        ];
    }
}
