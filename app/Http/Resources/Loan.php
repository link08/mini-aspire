<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Loan extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_full_name' => $this->user->full_name,
            'title' => $this->title,
            'description' => $this->description,
            'frequency' => $this->frequency,
            'installaments_number' => $this->installaments_number,
            'amount' => $this->amount,
            'interest_rate' => $this->interest_rate,
            'arrangement_fee' => $this->arrangement_fee,
            'total_amount' => $this->total_amount,
            'total_paid' => $this->total_paid,
            'total_unpaid' => $this->total_unpaid,
        ];
    }

    public function with($request)
    {
        return [
            'code' => 200,
            'message' => 'Ok',
        ];
    }
}
