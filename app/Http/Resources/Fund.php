<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Fund extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author' => $this->author,
            'description' => $this->description,
            'amount' => $this->amount,
        ];
    }

    public function with($request)
    {
        return [
            'code' => 200,
            'message' => 'Ok',
        ];
    }
}
