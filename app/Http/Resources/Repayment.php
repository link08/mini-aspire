<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Repayment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'loan' => $this->loan->title,
            'title' => $this->title,
            'description' => $this->description,
            'amount' => $this->amount,
            'due_date' => $this->due_date->formatLocalized('%e %b %Y'),
            'payment_method_id' => $this->payment_method,
            'paid_at' => $this->arrangement_fee,
        ];
    }

    public function with($request)
    {
        return [
            'code' => 200,
            'message' => 'Ok',
        ];
    }
}
