<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RepaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loan_id' => 'required|exists:loans,id',
            'title' => 'required',
            'description' => 'nullable',
            'amount' => 'required|numeric',
            'due_date' => 'required|numeric|min:1',
            'payment_method_id' => 'numeric',
        ];
    }

    public function attributes()
    {
        return [
            'loan_id' => 'Loan',
            'title' => 'Title',
            'description' => 'Description',
            'amount' => 'Amount',
            'due_date' => 'Due date',
            'payment_method_id' => 'Payment method',
        ];
    }
}
