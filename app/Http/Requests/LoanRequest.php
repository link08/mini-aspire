<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required|exists:users,id',
            'title' => 'required',
            'description' => 'nullable',
            'installaments_number' => 'required|numeric',
            'frequency' => 'required|numeric',
            'amount' => [
                'required',
                'numeric',
                'min:1',
                function($attribute, $value, $fail) {
                    if ($value > \App\Fund::getResidualAmount()) {
                        $fail($attribute.' not available.');
                    }
                }
            ],
            'interest_rate' => 'required|numeric',
            'arrangement_fee' => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'user_id' => 'User',
            'title' => 'Title',
            'description' => 'Description',
            'installaments_number' => 'Installaments number',
            'frequency' => 'Frequency',
            'amount' => 'Amount',
            'interest_rate' => 'Interest rate',
            'arrangement_fee' => 'Arrangement fee'
        ];
    }
}
