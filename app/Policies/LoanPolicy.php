<?php

namespace App\Policies;

use App\User;
use App\Loan;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Loan $loan)
    {
        return $user->is_superuser || $loan->user_id == $user->id;
    }

    public function create(User $user)
    {
        return $user->is_superuser;
    }

    public function update(User $user, Loan $loan)
    {
        return $user->is_superuser;
    }

    public function delete(User $user, Loan $loan)
    {
        return $user->is_superuser;
    }

}
