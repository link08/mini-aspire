<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function view(User $user, User $item)
    {
        return $user->is_superuser || $item->id == $user->id;
    }

    public function create(User $user)
    {
        return $user->is_superuser;
    }

    public function update(User $user, User $item)
    {
        return $user->is_superuser || $item->id == $user->id;
    }

    public function delete(User $user, User $item)
    {
        return $user->is_superuser;
    }

    public function performSuperuserAction(User $user)
    {
        return $user->is_superuser;
    }

}
