<?php

namespace App\Policies;

use App\User;
use App\Repayment;
use Illuminate\Auth\Access\HandlesAuthorization;

class RepaymentPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Repayment $repayment)
    {
        return $user->is_superuser || $repayment->loan->user_id == $user->id;
    }

    public function create(User $user)
    {
        return $user->is_superuser;
    }

    public function update(User $user, Repayment $repayment)
    {
        return $user->is_superuser;
    }

    public function delete(User $user, Repayment $repayment)
    {
        return $user->is_superuser;
    }

    public function pay(User $user, Repayment $repayment)
    {
        return $user->is_superuser || $repayment->loan->user_id == $user->id;
    }

}
