<?php

namespace App\Policies;

use App\User;
use App\Fund;
use Illuminate\Auth\Access\HandlesAuthorization;

class FundPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Fund $fund)
    {
        return $user->is_superuser;
    }

    public function create(User $user)
    {
        return $user->is_superuser;
    }

    public function update(User $user, Fund $fund)
    {
        return $user->is_superuser;
    }

    public function delete(User $user, Fund $fund)
    {
        return $user->is_superuser;
    }

}
