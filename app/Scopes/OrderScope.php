<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Support\Facades\Gate;

class OrderScope implements Scope
{
    private $column;
    private $direction;

    public function __construct($column, $direction = null)
    {
        $this->column = $column;
        $this->direction = $direction;
    }

    public function apply(Builder $builder, Model $model)
    {
        if (is_array($this->column)) {
            foreach ($this->column as $key => $direction) {
                $builder->orderBy($key, $direction);
            }
        }
        else {
            $direction = $this->direction ?: 'asc';
            $builder->orderBy($this->column, $direction);
        }
    }
}
