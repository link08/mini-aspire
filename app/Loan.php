<?php

namespace App;

use App\Scopes\OrderScope;
use App\Events\LoanCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 'title', 'description', 'frequency', 'installaments_number',
        'amount', 'interest_rate', 'arrangement_fee'
    ];

    protected $dispatchesEvents = [
        // Generate repayment installments when a loan is created
        'created' => LoanCreated::class,
    ];

    protected static function boot()
    {
        parent::boot();

        // Order by creation date
        static::addGlobalScope(new OrderScope('created_at', 'asc'));

        // Show only the abailable Element
        static::addGlobalScope('available', function(Builder $builder) {
            $user = auth()->user();
            if ( $user && ! $user->is_superuser ) {
                $builder->where('user_id', $user->id);
            }

            return $builder;
        });
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function repayments()
    {
        return $this->hasMany('App\Repayment');
    }

    public function getTotalAmountAttribute()
    {
        return $this->amount + $this->arrangement_fee + ($this->amount * $this->interest_rate / 100);
    }

    public function getTotalPaidAttribute()
    {
        return round((float) $this->repayments()->paid()->get()->sum('amount'), 2);
    }

    public function getTotalUnpaidAttribute()
    {
        return round((float) $this->repayments()->unpaid()->get()->sum('amount'), 2);
    }

    static function getTotalAmount()
    {
        return self::get()->sum('amount');
    }

}
