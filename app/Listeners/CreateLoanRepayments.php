<?php

namespace App\Listeners;

use App\Events\LoanCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateLoanRepayments
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LoanCreated  $event
     * @return void
     */
    public function handle(LoanCreated $event)
    {
        $loan = $event->loan;

        // Generate automatically the loan repayments
        $amount = (float)  $loan->total_amount / $loan->installaments_number;
        $dueDate = \Carbon\Carbon::now()->addDays($loan->frequency);

        if ($loan->installaments_number == 1) {
            $loan->repayments()->create([
                'title' => $loan->title . ' / unique installment',
                'amount' => $amount,
                'due_date' => $dueDate
            ]);

            return true;
        }
        foreach (range(1, $loan->installaments_number) as $i) {
            $loan->repayments()->create([
                'title' => $loan->title . ' / installment ' . $i,
                'amount' => $amount,
                'due_date' => $dueDate
            ]);
            $dueDate->addDays($loan->frequency);
        }

        return true;

    }
}
