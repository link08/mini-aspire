<?php

use Faker\Generator as Faker;

$factory->define(App\Loan::class, function (Faker $faker) {
    $userIds = \App\User::where('is_superuser', 0)->get()->pluck('id');
    $amount =  $faker->randomNumber(2);
    $arrangementFee = (100 - $faker->numberBetween(1, 15)) * $amount / 100;

    return [
        'user_id' => $faker->randomElement($userIds),
        'title' => 'Loan ' . $faker->numberBetween(1, 999),
        'frequency' => $faker->numberBetween(6, 36),
        'installaments_number' => $faker->numberBetween(6, 36),
        'amount' => $faker->numberBetween(6, 36),
        'interest_rate' => $faker->numberBetween(1, 15),
        'arrangement_fee' =>  $arrangementFee,
    ];
});
