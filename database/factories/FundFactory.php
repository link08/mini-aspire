<?php

use Faker\Generator as Faker;

$factory->define(App\Fund::class, function (Faker $faker) {
    return [
        'author' => $faker->name,
        'amount' => $faker->randomNumber(2),
    ];
});
