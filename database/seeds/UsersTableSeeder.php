<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'first_name' => 'Loris',
            'last_name' => 'Lunardi',
            'email' => 'lunardi.loris@gmail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('test'),
            'is_superuser' => true,
        ]);
    }
}
