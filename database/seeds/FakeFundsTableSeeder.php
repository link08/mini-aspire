<?php

use Illuminate\Database\Seeder;

class FakeFundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bar = $this->command->getOutput()->createProgressBar(20);

        factory(App\Fund::class, 20)
            ->create()
            ->each( function($fund) use ($bar) {
                $bar->advance();
            });

        $bar->finish();
        $this->command->info(PHP_EOL);
    }
}
