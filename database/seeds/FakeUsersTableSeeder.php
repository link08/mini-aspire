<?php

use Illuminate\Database\Seeder;

class FakeUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bar = $this->command->getOutput()->createProgressBar(10);

        factory(App\User::class, 10)
            ->create()
            ->each( function($user) use ($bar) {
                $bar->advance();
                for ($i = 1; $i <= rand(1,2); $i++) {
                    $user->loans()->save(factory(App\Loan::class)->make());
                }
            });

        $bar->finish();
        $this->command->info(PHP_EOL);
    }
}
