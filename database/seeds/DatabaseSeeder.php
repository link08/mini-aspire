<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);

        if (App::environment('local')) {
            $this->call(FakeFundsTableSeeder::class);
            $this->call(FakeUsersTableSeeder::class);
        }
    }
}
